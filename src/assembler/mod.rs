use std::fmt::{Debug, Formatter};
use std::fmt;
use crate::machine::Memory;
use crate::machine::op_codes::OpCodes;

pub fn assembly_to_binary(assembly: &str) -> Memory {
    if !assembly.is_ascii() {
        panic!("Needs to be ascii!");
    }

    let mut out = [0x6969_u16; crate::NUMBER_OF_ADDRESSES];

    let lines_iter = assembly.lines();
    for line in lines_iter {
        let commentless = line.split(';').next().unwrap();
        let line_pieces: Vec<String> = commentless.split_ascii_whitespace().map(String::from).collect();
        if line_pieces.len() == 0 {
            // empty line, nothing useful
            continue;
        }
        dbg!(&line_pieces);
        match line_pieces.get(0).unwrap().to_ascii_lowercase().as_str() {
            ".orig" => {
                let location = line_pieces.get(1).unwrap();
                out[0] = num_str_to_isize(location.as_str()) as u16;
            },
            "add" => {
                unimplemented!()
            },
            _thing => {
                panic!("Unknown opcode: {:?}", _thing);
            }
        }
        dbg_mem(out);
    }

    out
}

fn dbg_mem(m: Memory) {
    for (idx, dat) in m.iter().enumerate() {
        if *dat != 0x6969_u16 {
            println!("[{:#06x}] {:#06x}", idx, dat);
        }
    }
}

fn num_str_to_isize(s: &str) -> isize {
    let first = s.chars().next().unwrap().to_ascii_lowercase();
    if first == 'x' {
        // is hex
        return isize::from_str_radix(&s[1..], 16).unwrap();
    }
    if first == '#' {
        // decimal
        return isize::from_str_radix(s, 10).unwrap();
    }
    panic!("Number does not have needed prefix")
}
