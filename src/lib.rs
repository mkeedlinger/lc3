pub mod assembler;
pub mod machine;

pub const NUMBER_OF_ADDRESSES: usize = 65_536;
