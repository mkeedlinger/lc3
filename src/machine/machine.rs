use crate::machine::Memory;
use crate::machine::registers::Registers;

pub struct Machine {
    memory: Memory,
    registers: Registers,
    halted: bool,
}

impl Machine {
    pub fn new(program: Memory) -> Machine {
        Machine {
            halted: false,
            memory: program,
            registers: Registers {
                general: [0u16; 8],
                program_counter: program[0],
                condition_flags: Default::default()
            }
        }
    }

    pub fn tick(&mut self) {
        // get instruction
        let current_instruction = self.memory[self.registers.program_counter];
        println!("{:b}", op_code);

        // increment pc
        self.registers.program_counter += 1;

        // get op_code
        let op_code = current_instruction >> 12 as u8;
        println!("{:b}", op_code);
    }
}
