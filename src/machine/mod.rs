use crate::NUMBER_OF_ADDRESSES;

pub mod registers;
pub mod op_codes;
pub mod machine;

pub(crate) type Memory = [u16; NUMBER_OF_ADDRESSES];
