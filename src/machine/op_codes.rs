use std::fmt;

#[repr(u8)]
#[derive(Debug)]
pub enum OpCodes {
    Add = 0b_0001,
}

impl fmt::Display for OpCodes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
