#[derive(Default, Debug)]
pub struct Registers {
    pub(crate) general: [u16; 8],
    pub(crate) program_counter: u16,
    pub(crate) condition_flags: Condition,
}

#[derive(Debug)]
pub enum Condition {
    Positive,
    Zero,
    Negative,
}

impl Default for Condition {
    fn default() -> Self {
        Condition::Zero
    }
}
